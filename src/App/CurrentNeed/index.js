import React, { Component } from 'react'
import {
  Grid,
  Header,
  Progress,
  Button,
} from 'semantic-ui-react'
import './index.css'

export default class CurrentNeed extends Component {

  render() {
    return (
      <Grid>
        <Grid.Column mobile={16} tablet={8} computer={8} verticalAlign='bottom'>
          <div className={'current-need'}>
            <Header
              as='h2'
              style={this.headerText}>
              Current Need
            </Header>
    
            <div className={'current-need__data'}>
              <p>Total: <span>$131,294</span></p>
              <p>Remaining: <span>$2,805</span></p>
            </div>
    
            <Progress percent={97} color='green' progress />
          </div>
    
    
        </Grid.Column>
    
        <Grid.Column
          mobile={16}
          tablet={8}
          computer={8}
          verticalAlign='bottom'>
    
          <Button
            fluid
            color='green'
            size='massive'>
            GIVE
          </Button>
    
        </Grid.Column>
      </Grid>
    )
  }
}
