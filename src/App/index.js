import React, { Component } from 'react';
import { Container, Divider } from 'semantic-ui-react'
import './index.css';

// components
import Nav from './Nav';
import Carousel from './Carousel'
import MainStory from './MainStory'
import CurrentNeed from './CurrentNeed'
import StayConnected from './StayConnected'
import RelatedStories from './RelatedStories'
import FaqAccordion from './Accordion'

class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      
    }
  }

  render() {
    return (
      <div>
        <Nav />
        <Carousel/>
        <Container
          text
          style={{ marginTop: 3 + 'em' }}>
          <MainStory />
          <CurrentNeed />
          <Divider section/>
          <StayConnected />
          <Divider section hidden/>
          <RelatedStories/>
          <Divider section hidden/>
          <FaqAccordion />
          <Divider section hidden/>
        </Container>

      </div>
    );
  }
}


export default App;
