import React, { Component } from 'react';
import {
  Header,
  Embed
} from 'semantic-ui-react'
import GoogleMap from './GoogleMap'

export default class MainStory extends Component {
  headerText = {
    textTransform: 'uppercase'
  }

  render() {

    return (
      <div className='main-story'>
        <Header
          as='h2'
          style={this.headerText}
          content='The People'>
        </Header>
        <div className='main-story__para-map'>
          <GoogleMap />
          <p className='main-story__people'>The Kivira are fishermen who dwell near the “Pond of Electric Fish.” They’ve passed down their culture from generation to generation through songs and proverbs. The people have no published Scripture in their language, although translators drafted the whole Bible. This project will make God’s Word available to them in print.</p>
        </div>
        <Header
          as='h2'
          style={this.headerText}
          content='The Project'>
        </Header>
        <p className='main-story__project'>Ut sollicitudin lacus in turpis convallis, id ultrices turpis aliquet. Sed ut ligula pellentesque, blandit massa ut, molestie dolor. Pellentesque placerat lorem turpis, at tristique urna sollicitudin a. Nullam rhoncus nunc blandit augue vestibulum porta. Nullam tincidunt sagittis sem. Phasellus iaculis ipsum sit amet pellentesque bibendum. Integer nec rutrum turpis. Nullam eget neque et purus vestibulum fringilla a laoreet libero. Fusce a pharetra metus. Sed consectetur nibh sit amet placerat mollis. Nam at magna vitae magna sodales convallis eget id leo. Donec ut blandit lorem, ac mollis ligula. Quisque pellentesque ex ex, vel ornare turpis luctus a. Integer sit amet lacus dictum, volutpat est vel, rhoncus metus. Nullam vulputate nisi eros, in eleifend mi varius sed. In convallis lobortis arcu, ac vulputate massa sagittis nec.</p>
      </div>
    )
  }
}

