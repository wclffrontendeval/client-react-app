import React, { Component } from 'react'
import { Embed } from 'semantic-ui-react'
import './index.css'

export default class GoogleMap extends Component {
  render () {
    return (
      <Embed
        active={true}
        className='google-map'
        url='https://www.google.com/maps/embed/v1/view?key=AIzaSyDmj0lazGHyXUwqL7-3q42c_vgDVINfUvU&center=0.28544300,29.19805900&zoom=10'
      />
    )
  }
}