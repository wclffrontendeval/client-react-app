import React, { Component }from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import { Image } from 'semantic-ui-react'
import 'pure-react-carousel/dist/react-carousel.es.css'
import girlscover from './girls-cover.jpg'
 
export default class Carousel extends Component {
  render() {
    return (
      <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={50}
        totalSlides={3}
      >        
        <Slider>
          <Slide index={0}>
            <img src={girlscover}/>
          </Slide>
          <Slide index={1}>I am the second Slide.</Slide>
          <Slide index={2}>I am the third Slide.</Slide>
        </Slider>
        <ButtonBack>Back</ButtonBack>
        <ButtonNext>Next</ButtonNext>
      </CarouselProvider>
    );
  }
}