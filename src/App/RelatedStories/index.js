import React, { Component } from 'react'
import {
 Grid,
 Card
} from 'semantic-ui-react'

export default class RelatedStories extends Component {
  render () {
    return (
      <Grid stackable columns={2}>
        <Grid.Column>
          <Card
          fluid
          image='http://via.placeholder.com/300x200'
          header='Bear claw cookie gummies danish. Pie cookie bear claw'
          meta='Foat cake halvah gummies sweet'
          description='Cake jelly croissant bear claw brownie bear claw tiramisu gummi bears bear claw. Halvah chocolate cake brownie caramels brownie cheesecake soufflé. Tart bear claw lemon drops pie pastry cupcake danish sesame.'
          />
        </Grid.Column>
        <Grid.Column>
          <Card
            fluid
            image='http://via.placeholder.com/300x200'
            header='Bear claw cookie gummies danish. Pie cookie bear claw'
            meta='Foat cake halvah gummies sweet'
            description='Cake jelly croissant bear claw brownie bear claw tiramisu gummi bears bear claw. Halvah chocolate cake brownie caramels brownie cheesecake soufflé. Tart bear claw lemon drops pie pastry cupcake danish sesame.'
            />
        </Grid.Column>
      </Grid>
    )
  }
}