import React, { Component } from 'react'
import { Menu } from 'semantic-ui-react'

export default class Nav extends Component {
  state = {}

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Menu 
        borderless 
        inverted 
        size='huge' 
        color='olive'
        style={{borderRadius: 0}}>
        <Menu.Item header>Wycliffe</Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item name='jobs' active={activeItem === 'jobs'} onClick={this.handleItemClick} />
          <Menu.Item name='locations' active={activeItem === 'locations'} onClick={this.handleItemClick} />
        </Menu.Menu>
      </Menu>
    )
  }
}