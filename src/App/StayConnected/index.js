import React, { Component } from 'react'
import {
  Header,
  Form,
  Input
} from 'semantic-ui-react'
import './index.css'

export default class StayConnected extends Component {
  render() {
    return (
      <div className='stay-connected'>
        <Header
          as='h1'
          content={'Stay Connected With Wycliffe\'s Work'}
          textAlign='center' 
          className='stay-connected__header'
        />

        <Form>
          <Form.Group widths='equal'>
            <Form.Field control={Input} label='First name' placeholder='First name' />
            <Form.Field control={Input} label='Last name' placeholder='Last name' />
            <Form.Field control={Input} label='Email' placeholder='Email' />
            <Form.Button content='Submit' fluid className='stay-connected__submit-button'/>
          </Form.Group>
        </Form>
      </div>
    )
  }
}